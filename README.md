![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

[TOC]

# Inleiding

Saxion gebruikt de Open Onderwijs API (OOAPI) voor het beschrijven van het CDM en de ontsluiting van de onderwijsstructuur tussen bron- en doelsystemen of apps. OOAPI is een initiatief van SURF en wordt doorontwikkeld door een werkgroep met mensen van SURF zelf en mensen van verschillende hogescholen. Meer informatie over OOAPI is te vinden op de website: https://openonderwijsapi.nl/.

Ten behoeve van de deelname aan de SURF proof of concept onderwijsaanbod-data wordt deze documentatie open-source aangeboden zodat andere onderwijsinstellingen hier van kunnen leren bij de implementatie van OOAPI. Deze documentatie is geschreven om de ESB ontwikkelaars te voorzien van specificaties.

Naast de basis principes van de OOAPI implementatie bij Saxion worden in dit document ook de specifieke implementaties van elke endpoint beschreven met de eventuele samenhang naar andere endpoints.

# Basisprincipes

## HATEOAS

De OOAPI is ontwikkeld volgens het principe: Hypermedia as the Engine of Application State (HATEOAS) (zie ook: https://en.wikipedia.org/wiki/HATEOAS). Dit komt er op neer dat de OOAPI RESTful is en gebruikt maakt van links in de responses waarmee de client als het ware kan bladeren door de gegevens in de OOAPI.

Standaard zal de Saxion OOAPI voorzien worden van de verplichte `self`-link en (indien van toepassing) `prev` en `next` links om paginering te ondersteunen.

## Query parameters

Indien de endpoint dit volgens OOAPI ondersteund, zal Saxion paginering-parameters ondersteunen: `pageSize` en `pageNumber`. In relatie met de `prev` en `next`-link kan er RESTful door de data gebladerd worden.

Eventuele overige parameters worden per endpoint beschreven.

## Responses

Alle succesvolle responses worden volgens het schema (en content-type) `application/hal+json` teruggeven. Fout-responses worden teruggegeven volgens het schema (en content-type) `application/problem+json` (zie foutafhandeling).


## Basis endpoint

De basis endpoint voor de OOAPI bij Saxion is per omgeving anders. Onderstaande basis endpoints worden verder in het document niet vermeld. Bij overige verwijzingen naar een endpoint zal het gaan om een relatief pad vanaf de basis endpoint.

**Test/Acceptatie:** `https://.../ooapi/v3/`

**Productie:** `https://.../ooapi/v3/`


## OpenAPI Specification

De geïmplementeerde services dienen te worden gespecificeerd middels OpenAPI specification v3 (https://github.com/OAI/OpenAPI-Specification/). Deze specificatie moet ook beschikbaar zijn via een URL. Deze URL wordt in de service-metadata endpoint ook teruggegeven als verplicht veld.

**URL:** `/docs/spec.yaml`


De `spec.yaml` zal een basis beschrijving omvatten met informatie over privacy en gebruikscondities. Verder zullen de specs van specifieke endpoints geïmporteerd worden vanuit de door de ESB gegenereerde specificaties.


Om de specificatie te publiceren als leesbare documentatie, wordt gebruik gemaakt van ReDoc (https://github.com/Redocly/redoc). Op de `/docs` url wordt een standaard HTML pagina gepresenteerd met de volgende bron code.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>ReDoc</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/cssfamily=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">
    <style>
      body {
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <redoc spec-url='spec.yaml'></redoc>
    <script src="https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"> </script>
  </body>
</html>
```

## Leerjaren

Elk leerjaar kan de onderwijscatalogus anders opgebouwd zijn. De OOAPI heeft standaard geen oplossing om met meerdere leerjaren te werk te gaan. Om er voor te zorgen dat de Saxion OOAPI met leerjaren overweg kan zonder afbreuk te doen aan de standaard, zal er gewerkt gaan worden met HTTP-Headers om in de aanvraag te sturen op een specifiek studiejaar. Mocht deze HTTP-Header niet gebruikt worden, wordt er uitgegaan van het huidige studiejaar.

De HTTP-Header om het leerjaar te sturen is al volgt: `X-Academic-Year`. Deze header kan een waarde hebben conform een leerjaar aanduiding, bijvoorbeeld 2019-2020 (`/^\d{4}-\d{4}$/`).

Het huidige leerjaar wordt bepaald op basis van de huidige datum. Vanaf 1 augustus gaat het nieuwe leerjaar in. Bijvoorbeeld:

- 31-07-2019 = 2018-2019
- 01-08-2019 = 2019-2020

# Afwijkingen

Om OOAPI in te passen bij Saxion en dienend aan het informatiemodel van Saxion, zijn er uitbreidingen of aanpassingen gedaan bij de implementatie van OOAPI. Onderstaand overzicht geeft aan welke aanpassingen er zijn gedaan voor welk doel.

| Aanpassing                                      | Doel                                                         |
| ----------------------------------------------- | ------------------------------------------------------------ |
| Link `faculty` bij `educationalProgramme`       | Bij Saxion is er een link tussen een Academie (Faculty) en een Opleidingsvariant (EducationalProgramme). Om te kunnen zoeken naar opleidingsvarianten van een bepaalde academie is dit filter bij Saxion als extra toegevoegd. |
| Link `educationalProgrammes` bij `course-group` | Initieel ondersteund de OOAPI geen link tussen `educational-programmes` en `course-groups`. Door de mapping van bijvoorbeeld clusters en (post-)propedeuses op `course-groups` is er bij Saxion wel degelijk de wens om `educational-programmes` (opleidingsvarianten)  aan `course-groups` te koppelen. Zo wordt het mogelijk `course-groups` te filteren op basis van een `educationalProgrammeId`. |
| Student query-parameter bij `news-items`        | Standaard zijn `news-items` uit de OOAPI niet gepersonaliseerd. Saxion wil deze endpoint echter wel gebruiken om persoonlijke mededelingen te gaan tonen uit bijvoorbeeld de ELO en het SVS. Daarom wordt deze (optionele) query parameter toegevoegd conform andere endpoints welke wel een student-query-parameter hebben. |
| Geen array’s met objecten in list-responses     | Volgens OOAPI bestaat een object van een bepaald type (bijvoorbeeld een `course`) uit velden behorende aan het object met daarnaast een `_links` sectie. In deze `_links` sectie worden soms links gebruikt met een array van gegevens (bijvoorbeeld `educationalProgrammes` bij een `course`). Omwille performance is er voor gekozen om dit soort `_links`-array’s niet op te nemen bij het ophalen van een lijst met objecten. |
| `_extend` block                                 | Bij verschillende endpoints wordt in het informatie object een `_extend` block gebruikt om extra Saxion velden aan te duiden welke niet logisch te mappen zijn op de standaard velden van OOAPI, maar wel van belang zijn voor uitwisseling van gegevens. |

# Definities Bronsystemen

De OOAPI haalt haar gegevens uit verschillende bronsystemen. Informatie over deze bronsystemen (met eventuele documentatie) worden in dit hoofdstuk beschreven.

## MDS

Bij Saxion draait een MDM oplossing van Microsoft: MDS (Master Data Services). Deze laag verzorgt businesslogica boven op een SQL Server 2016 database. Om volgens de juiste regels gegevens uit de MDS op te halen, zullen we gebruik maken van de MDS webservice. Documentatie van deze webservice zijn te vinden op de volgende links:

- https://docs.microsoft.com/en-us/dotnet/api/microsoft.masterdataservices?view=sqlserver-2016
- https://docs.microsoft.com/en-us/openspecs/sql_server_protocols/ms-ssmdsws
- https://github.com/microsoft/sql-server-samples/tree/master/samples/features/master-data-services/master-data

De webservices van MDS zijn gestoeld op SOAP inclusief WSDL.

| Omgeving       | WSDL                                             |
| -------------- | ------------------------------------------------ |
| **A**cceptatie | `https://.../MDS/Service/Service.svc?singleWsdl` |
| **P**roductie  | `https://.../MDS/Service/Service.svc?singleWsdl` |

## ODS

De ODS is een Microsoft SQL Server omgeving welke voor een gehele OTAP straat beschikbaar is. In overleg wordt afgestemd voor welke test welke omgeving gebruikt wordt.

| Omgeving       | Host | Database     |
| -------------- | ---- | ------------ |
| **O**ntwikkel  | ...  | `cdm_data_o` |
| **T**est       | ...  | `cdm_data_t` |
| **A**cceptatie | ...  | `cdm_data_a` |
| **P**roductie  | ...  | `cdm_data_p` |

Authenticatie gegevens worden via beveiligd wachtwoord beheer gedeeld met ontwikkelaars.

In de database bevind zich een schema `oc`. Dit is de plek waar de tabellen van de onderwijscatalogus geplaatst zijn. De tabellen zijn conform CDM en afgeleid van de entiteiten en velden van de OOAPI.

![ods_erd](ods_erd.png)

# Foutafhandeling

## Triggers

| Foutafhandeling trigger                                      | Proces stoppen | Bericht uit queue | Automatisch afmelden |
| ------------------------------------------------------------ | :------------: | :---------------: | :------------------: |
| Connection timeout                                           |       J        |        Nvt        |          N           |
| Response anders dan verwacht (Webservice response anders dan voorbeelden of HTTP anders dan 200) |       J        |        Nvt        |          N           |

## Instellingen

| Instelling           | Standaard waarde |
| -------------------- | ---------------- |
| Categorie            | OOAPI            |
| Target resolve-hours | 120              |

## Response

**`HTTP 400`**
Zodra er een request wordt gedaan welke (mogelijk) een internal server error zal triggeren, wordt er aan de client teruggegeven, dat de request niet correct is. Dit gebeurt bijvoorbeeld bij verkeerde input van het pagina nummer (`pageNumber=0`).

```json
{
  "status": 400
  "title": "Bad request"
}
```

**`HTTP 404`**
Zodra er een pagina niet gevonden (bijvoorbeeld verkeerde parameters of een niet bestaande pagina), wordt er geen foutafhandeling afgetrapt, maar wordt er alleen een `HTTP 404` teruggegeven met een JSON response (`application/problem+json`):

```json
{
  "status": 404,
  "title": "Resource not found"
}
```

`HTTP 500`
Mocht er in de backend iets mis gaan dan wordt foutafhandeling wel afgetrapt. Dit kan beantwoord worden met een `HTTP 500` response met een JSON payload (`application/problem+json`):

```json
{
  "status": 500,
  "title": "Internal Server Error"
}
```

# Endpoints

- [service-metadata](./endpoints/service-metadata.md)
- [institution](./endpoints/institution.md)
- [educatoinal-programmes](./endpoints/educational-programmes.md)
- [courses](./endpoints/courses.md)
- [course-offerings](./endpoints/course-offerings.md)

