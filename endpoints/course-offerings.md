![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

# OOAPI: course-offerings

**Endpoint-list:** `/course-offerings`
**Endpoint-single**: `/courses-offering/{courseOfferingId}`
**Authenticatie:** ja

De course-offering endpoints geven course-offerings (kwartielen) terug op basis van eventueel meegegeven filters.

## Ophalen lijst

```sequence
participant Client
participant APIGW
participant ODS

Client->APIGW: GET: ooapi/course-offerings
APIGW->ODS: Query: course_offerings
ODS-->APIGW: Query-result
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(course-offerings)
```

De volgende query parameters worden ondersteund (conform OOAPI specificatie):

- `pageSize`
- `pageNumber`
- `course`

Standaard sortering: `id`

**Request (Client->OOAPI)**

```http
GET https://.../ooapi/course-offerings/?pageSize=100&course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2 HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Query (OOAPI->ODS (`course_offering`))**
In het voorbeeld wordt `TOP 100` aangepast conform de query parameter `pageSize`.

```mssql
SELECT TOP 100 co.id AS courseOffering_id, co.academic_year, co.period, c.*,
	formats = STUFF((SELECT ',' + format FROM oc.course_format AS cf WHERE cf.course_id = c.id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
FROM oc.course_offering AS co
	JOIN oc.course AS c ON c.id = co.course_id
WHERE 1 = 1
-- Optioneel filter course
AND course_id = '01f8ab2d-6edf-44b4-99f5-c692f326f0e2'
ORDER BY co.id
```

**Query-result**

```
courseOffering_id|academic_year|period    |id                       
-----------------|-------------|----------|------------------------- ...
             1781|             |Kwartiel 4|01f8ab2d-6edf-44b4-99f5-c ...
             5452|             |Kwartiel 3|01f8ab2d-6edf-44b4-99f5-c ...
...
```

**Response (OOAPI)**

- `_embedded/items` worden gemapped conform mapping. Elk course-offering item heeft een course-object waarin de gehele course weer gemapped wordt. Hier worden net zoals bij de course-list ook geen educationalProgrammes en courseGroups in gemapped als lijst.
- `_links` worden gevuld met automatisch gegenereerde href's conform paginering. Hierin is (volgens OOAPI specs) een `self`, `next` en `prev` object mogelijk, afhankelijk van de beschikbare pagina’s. Genoemde href’s zijn relatief aan het vaste gedeelte van het endpoint. Alle query parameters van de originele request dienen verwerkt te worden in de nieuwe href’s.

```json
{
  "pageSize": 100,
  "pageNumber": 1,
  "_embedded": {
    "items": [
      {
        "courseOfferingId": "1781",
        "course": {
          "courseId": "01f8ab2d-6edf-44b4-99f5-c692f326f0e2",
          "name": "Bedrijfsadministratie 2.3 & 2.4",
          "abbreviation": "L.11841",
          "ects": 0,
          "description": "Zie Blackboard",
          "learningOutcomes": "",
          "goals": "",
          "requirements": "",
          "level": "",
          "format": [
            "Practicum",
            "Hoorcollege",
            "Werkcollege"
          ],
          "modeOfDelivery": "",
          "mainLanguage": "nl-NL",
          "enrollment": "",
          "resources": "",
          "exams": "",
          "schedule": "",
          "link": "",
          "_links": {
            "self": {
              "href": "courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
            },
            "courseOfferings": {
              "href": "coure-offerings/?course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
            }
          }
        },
        "maxNumberStudents": 0,
        "currentNumberStudents": 0,
        "academicYear": "",
        "period": "Kwartiel 4",
        "_links": {
          "self": {
            "href": "course-offerings/1781"
          }
        }
      },
      {
        "courseOfferingId": "5452",
        "course": {
          "courseId": "01f8ab2d-6edf-44b4-99f5-c692f326f0e2",
          "name": "Bedrijfsadministratie 2.3 & 2.4",
          "abbreviation": "L.11841",
          "ects": 0,
          "description": "Zie Blackboard",
          "learningOutcomes": "",
          "goals": "",
          "requirements": "",
          "level": "",
          "format": [
            "Practicum",
            "Hoorcollege",
            "Werkcollege"
          ],
          "modeOfDelivery": "",
          "mainLanguage": "nl-NL",
          "enrollment": "",
          "resources": "",
          "exams": "",
          "schedule": "",
          "link": "",
          "_links": {
            "self": {
              "href": "courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
            },
            "courseOfferings": {
              "href": "coure-offerings/?course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
            }
          }
        },
        "maxNumberStudents": 0,
        "currentNumberStudents": 0,
        "academicYear": "",
        "period": "Kwartiel 3",
        "_links": {
          "self": {
            "href": "course-offerings/5452"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "course-offerings/?pageSize=100&course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
    }
  }
}
```

## Ophalen enkel record

```sequence
participant Client
participant APIGW
participant ODS

Client->APIGW: GET: ooapi/course-offerings/{courseOfferingId}
APIGW->ODS: Query: course_offering-by-id
ODS-->APIGW: Query-result
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(course-offering)
```

**Request (Client->OOAPI)**

```http
GET https://.../ooapi/course-offerings/1781 HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Query (OOAPI->ODS (`course_offering`))**

```mssql
SELECT co.id AS courseOffering_id, co.academic_year, co.period, c.*,
	formats = STUFF((SELECT ',' + format FROM oc.course_format AS cf WHERE cf.course_id = c.id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
FROM oc.course_offering AS co
	JOIN oc.course AS c ON c.id = co.course_id
WHERE co.id = 1781
```

**Query-result**

```
courseOffering_id|academic_year|period    |id                     
-----------------|-------------|----------|----------------------- ...
             1781|             |Kwartiel 4|01f8ab2d-6edf-44b4-99f5 ...
```

**Response (OOAPI)** 

- De course-offering heeft een course-object waarin de gehele course weer gemapped wordt. Hier worden net zoals bij de course-offering-list ook geen educationalProgrammes en courseGroups in gemapped als lijst.
- Genoemde href’s zijn relatief aan het vaste gedeelte van het endpoint.

```json
{
  "courseOfferingId": "1781",
  "course": {
    "courseId": "01f8ab2d-6edf-44b4-99f5-c692f326f0e2",
    "name": "Bedrijfsadministratie 2.3 & 2.4",
    "abbreviation": "L.11841",
    "ects": 0,
    "description": "Zie Blackboard",
    "learningOutcomes": "",
    "goals": "",
    "requirements": "",
    "level": "",
    "format": [
      "Practicum",
      "Hoorcollege",
      "Werkcollege"
    ],
    "modeOfDelivery": "",
    "mainLanguage": "nl-NL",
    "enrollment": "",
    "resources": "",
    "exams": "",
    "schedule": "",
    "link": "",
    "_links": {
      "self": {
        "href": "courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
      },
      "courseOfferings": {
        "href": "coure-offerings/?course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
      }
    }
  },
  "maxNumberStudents": 0,
  "currentNumberStudents": 0,
  "academicYear": "",
  "period": "Kwartiel 4",
  "_links": {
    "self": {
      "href": "course-offerings/1781"
    }
  }
}
```

## Mapping

| Bron (ODS)          | Datatransformatie                                            | OOAPI Veld              |
| ------------------- | ------------------------------------------------------------ | ----------------------- |
| `courseOffering_id` |                                                              | `courseOfferingId`      |
|                     | *Het course-object wordt gemapped volgens de mapping van de course API.* | `course`                |
|                     | Standaard waarde 0                                           | `maxNumberStudents`     |
|                     | Standaard waarde 0                                           | `currentNumberStudents` |
| `academic_year`     |                                                              | `academicYear`          |
| `period`            |                                                              | `period`                |
| `courseOffering_id` | Samenvoegen `course-offerings/{id}`                          | `_links/self/href`      |

