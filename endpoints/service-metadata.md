![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

# OOAPI: service-metadata

**Endpoint:** `/`
**Authenticatie:** nee

De service metadata endpoint zal standaard metadata informatie teruggeven over de API.

## Voorbeeld bericht

**Request (Client -> OOAPI)**

```http
GET https://.../ooapi/ HTTP/1.1
Accept: application/json
```

**Response (OOAPI)**

```json
{
    "owner": "Hogeschool Saxion",
    "logo": "https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png",
    "specification": "/docs/spec.yaml",
    "documentation": "/docs",
    "_links": {
        "self": {
        	"href": "/"
        },
        "endpoints": [
            {
                "href": "/persons"
            },
            {
                "href": "/faculties"
            },
            {
                "href": "/educational-departments"
            },
            {
                "href": "/educational-plans"
            }
        ]
    }
}
```

## Mapping

 Deze service wordt niet vanuit een bron systeem aanboden. De waardes welke teruggegeven worden door de service zijn vast waardes welke in de ESB te configureren en te muteren zijn.

| OOAPI Veld           | Waarde                                                       |
| -------------------- | ------------------------------------------------------------ |
| `owner`              | Hogeschool Saxion                                            |
| `logo`               | `https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png` |
| `specification`      | `/docs/spec.yaml`                                            |
| `documentation`      | `/docs`                                                      |
| `_links/self/href`   | `/`                                                          |
| `_links/endpoints/*` | *Voor elke geïmplementeerde endpoint wordt het relatieve pad hier weergegeven.* |

