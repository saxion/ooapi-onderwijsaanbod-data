

![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

# OOAPI: educational-programmes

**Endpoint-list:** `/educational-programmes`
**Endpoint-single:** `/educational-programmes/{educationalProgrammeId}`
**Authenticatie:** ja

De educational-programme endpoints geven educational-programmes (opleidingsvarianten) terug op basis van eventueel meegegeven filters.

## Ophalen lijst

```sequence
participant Client
participant APIGW
participant MDS

Client->APIGW: GET: ooapi/educational-programmes
APIGW->MDS: EntityMembersGet()
MDS-->APIGW: Response(sor_opleidingen)
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(education-programmes)
```

De volgende query parameters worden ondersteund (conform OOAPI specificatie):

- `pageSize`
- `pageNumber`
- `order`
- `faculty` *(OOAPI specificatie afwijking)*

Standaard sortering: `id`

**Request (Client->OOAPI)**

```http
GET https://.../ooapi/educational-programmes/?pageSize=100&order=educationalProgrammeId HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Request (OOAPI->MDS)**
De query parameters `pageSize` en `pageNumber` kunnen 1 op 1 overgenomen worden op resp. de velden `PageSize` en `PageNumber` in de MDS-request.

Sorteren met de query parameter `order` is optioneel. Als deze is meegegeven, wordt er een `SortColumnId` object meegegeven in de request naar de MDS webservice met daarin een `Id` veld. De mapping voor de sorting is als volgt:

| OOAPI veld               | MDS `<Id></Id>` vulling                |
| ------------------------ | -------------------------------------- |
| `educationalProgrammeId` | `41bc8e6b-1f54-4538-b194-eb573203edbc` |
| `name`                   | `8dec398d-2685-47dc-84fd-892122ff3a4a` |
| `description`            | `a773e70c-7b35-468f-80c7-23006d154f25` |
| `termStartDate`          | `a83330d3-18dc-4f0d-a738-efdb74f19eb3` |
| `fieldsOfStudy`          | `6d40ad65-762f-46bf-8d35-b80a1373b004` |
| `modeOfStudy`            | `a6a7ec40-c9e1-4838-99ba-67b182195afe` |

Mocht er een veld meegegeven worden in de query parameter `order` welke niet in bovenstaande mapping bestaat, zal er een `HTTP 404` error gegeneerd worden, zonder deze fout door te zetten naar de generieke foutafhandeling.

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://www.w3.org/2005/09/ws-i18n" xmlns:ns="http://schemas.microsoft.com/sqlserver/masterdataservices/2009/09">
   <soapenv:Header>
      <ws:International/>
   </soapenv:Header>
   <soapenv:Body>
      <ns:EntityMembersGetRequest>
         <ns:MembersGetCriteria>
            <ns:EntityId>
               <ns:Name>Opleidingen</ns:Name>
            </ns:EntityId>
            <ns:MemberReturnOption>DataAndCounts</ns:MemberReturnOption>
            <ns:MemberType>Leaf</ns:MemberType>
            <ns:ModelId>
               <ns:Name>Things</ns:Name>
            </ns:ModelId>
            <ns:PageNumber>1</ns:PageNumber>
            <ns:PageSize>100</ns:PageSize>
            <ns:SortColumnId>
               <ns:Id>41bc8e6b-1f54-4538-b194-eb573203edbc</ns:Id>
            </ns:SortColumnId>
            <ns:VersionId>
               <ns:Name>VERSION_1</ns:Name>
            </ns:VersionId>
         </ns:MembersGetCriteria>
      </ns:EntityMembersGetRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

**Request met filter (OOAPI->MDS)**
Indien het `faculty` filter in de query parameters meegegeven is, kan deze als `SearchTerm` meegegeven worden in de request naar MDS. Het formaat van dit filter is als volgt:

`[Academienaam afkorting] = '{facultyId}'`

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://www.w3.org/2005/09/ws-i18n" xmlns:ns="http://schemas.microsoft.com/sqlserver/masterdataservices/2009/09">
   <soapenv:Header>
      <ws:International/>
   </soapenv:Header>
   <soapenv:Body>
      <ns:EntityMembersGetRequest>
         <ns:MembersGetCriteria>
            <ns:EntityId>
               <ns:Name>Opleidingen</ns:Name>
            </ns:EntityId>
            <ns:MemberReturnOption>DataAndCounts</ns:MemberReturnOption>
            <ns:MemberType>Leaf</ns:MemberType>
            <ns:ModelId>
               <ns:Name>Things</ns:Name>
            </ns:ModelId>
            <ns:PageNumber>1</ns:PageNumber>
            <ns:PageSize>100</ns:PageSize>
            <ns:SearchTerm>[Academienaam afkorting] = 'LED'</ns:SearchTerm>
            <ns:SortColumnId>
               <ns:Id>41bc8e6b-1f54-4538-b194-eb573203edbc</ns:Id>
            </ns:SortColumnId>
            <ns:VersionId>
               <ns:Name>VERSION_1</ns:Name>
            </ns:VersionId>
         </ns:MembersGetCriteria>
      </ns:EntityMembersGetRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

**Response (MDS)**
De response bevat een object `Members` met daarin een `Member` object per opleiding. Attributen van de opleidingen staan binnen het `Attributes` object, elk in een `Attribute` object. Het voorbeeld is afgekapt op de plekken waar dat met XML-comment bij is gezet.

```xml
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
   <s:Body xmlns:a="http://www.w3.org/2001/XMLSchema">
      <EntityMembersGetResponse xmlns="http://schemas.microsoft.com/sqlserver/masterdataservices/2009/09">
         <EntityMembers xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <EntityId>
               <Name>Opleidingen</Name>
            </EntityId>
            <MemberType>Leaf</MemberType>
            <Members>
               <Member>
                  <AccessPermission>Update</AccessPermission>
                  <Attributes>
                     <Attribute>
                        <Identifier>
                           <Id>758da61c-7f1b-4078-a242-da577b336988</Id>
                           <Name>mutatiedatum</Name>
                        </Identifier>
                        <Type>DateTime</Type>
                        <Value i:type="a:dateTime">2015-08-28T00:00:00</Value>
                     </Attribute>
                     <Attribute>
                        <Identifier>
                           <Id>a773e70c-7b35-468f-80c7-23006d154f25</Id>
                           <Name>naam opleiding</Name>
                        </Identifier>
                        <Type>String</Type>
                        <Value i:type="a:string">B Chemie</Value>
                     </Attribute>
                     <Attribute>
                        <Identifier>
                           <Id>8e2cc3ca-f213-4158-b0a9-65fcba85e847</Id>
                           <Name>afkorting opleiding</Name>
                        </Identifier>
                        <Type>String</Type>
                        <Value i:type="a:string">C</Value>
                     </Attribute>
                     <!-- ... meer attribute-objecten conform mapping ... -->
                  </Attributes>
                  <AuditInfo>
                     <CreatedDateTime>2018-04-09T15:15:38.76</CreatedDateTime>
                     <CreatedUserId/>
                     <RevisionId>272018</RevisionId>
                     <UpdatedDateTime>2018-08-07T11:51:03.593</UpdatedDateTime>
                     <UpdatedUserId>
                        <Name>DOMAIN\bbe15</Name>
                     </UpdatedUserId>
                  </AuditInfo>
                  <MemberId>
                     <Id>60989517-e91e-49d8-8563-65409391e914</Id>
                     <InternalId>1</InternalId>
                     <Code>1</Code>
                     <MemberType>Leaf</MemberType>
                  </MemberId>
                  <SecurityPermission>Access</SecurityPermission>
                  <ValidationStatus>ValidationSucceeded</ValidationStatus>
               </Member>
               <!-- ... meer member-objecten ... -->
            </Members>
            <ModelId>
               <Name>Things</Name>
            </ModelId>
            <VersionId>
               <Name>VERSION_1</Name>
            </VersionId>
         </EntityMembers>
         <EntityMembersInformation xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <MemberCount>100</MemberCount>
            <PageNumber>1</PageNumber>
            <TotalMemberCount>291</TotalMemberCount>
            <TotalPages>3</TotalPages>
         </EntityMembersInformation>
         <OperationResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <Errors/>
            <RequestId>2e6917b2-5e1d-4639-a5cc-47f473f2fcb2</RequestId>
         </OperationResult>
      </EntityMembersGetResponse>
   </s:Body>
</s:Envelope>
```

**Response (OOAPI)**

- Het `_links` object met paginering (`next` en `prev`) wordt opgebouwd middels de informatie uit de MDS-response in het blok `EntityMembersInformation`. 

```json
{
  "pageSize": "100",
  "pageNumber": "1",
  "_embedded": {
    "items": [
      {
        "educationalProgrammeId": "50002140",
        "name": "B Chemie",
        "description": "",
        "termStartDate": "2011-09-01",
        "termEndDate": "",
        "ects": 0,
        "mainLanguage": "nl-NL",
        "qualificationAwarded": "Bachelor of Science",
        "lenghtOfProgramme": 0,
        "levelOfQualification": "",
        "fieldsOfStudy": "Techniek",
        "profileOfProgramme": "",
        "programmeLearningOutcomes": "",
        "modeOfStudy": "voltijd",
        "_extend": {
          "code": "C1VT"
        },
        "_links": {
          "self": {
            "href": "educational-programmes/50002140"
          },
          "courses": {
            "href": "courses?educationalProgramme=50002140"
          }
        }
      },
      ... (NOG MAXIMAAL 99 ITEMS) ...
    ]
  },
  "_links": {
    "self": {
      "href": "educational-programmes/?pageSize=100"
    },
    "next": {
      "href": "educational-programmes/?pageSize=100&pageNumber=2"
    }
  }
}
```

## Ophalen enkel record

```sequence
participant Client
participant APIGW
participant MDS

Client->APIGW: GET: ooapi/educational-programmes/{id}
APIGW->MDS: EntityMembersGet(<filter>)
MDS-->APIGW: Response(sor_opleiding)
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(educational-programme)
```

**Request (Client->OOAPI)**

```http
GET https://.../ooapi/educational-programmes/50002140 HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Request (OOAPI->MDS)**
In de request op de OOAPI wordt het ID van de opleiding meegegeven (`50002140` in het voorbeeld). Deze waarde wordt vervolgens gemapped in de SOAP request naar MDS in het veld `SearchTerm`. Hierin staat een search query welke er voor zorgt dat er op het juiste veld gezocht wordt: `Object-Id = '{id}'`

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://www.w3.org/2005/09/ws-i18n" xmlns:ns="http://schemas.microsoft.com/sqlserver/masterdataservices/2009/09">
   <soapenv:Header>
      <ws:International>
      </ws:International>
   </soapenv:Header>
   <soapenv:Body>
      <ns:EntityMembersGetRequest>
         <ns:MembersGetCriteria>
            <ns:EntityId>
               <ns:Name>Opleidingen</ns:Name>
            </ns:EntityId>
            <ns:MemberReturnOption>Data</ns:MemberReturnOption>
            <ns:MemberType>Leaf</ns:MemberType>
            <ns:ModelId>
               <ns:Name>Things</ns:Name>
            </ns:ModelId>
            <ns:PageNumber>1</ns:PageNumber>
            <ns:PageSize>100</ns:PageSize>
            <ns:SearchTerm>Object-Id = '50002140'</ns:SearchTerm>
            <ns:VersionId>
               <ns:Name>VERSION_1</ns:Name>
            </ns:VersionId>
         </ns:MembersGetCriteria>
      </ns:EntityMembersGetRequest>
   </soapenv:Body>
</soapenv:Envelope>
```

**Response (MDS)**

```xml
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
   <s:Body xmlns:a="http://www.w3.org/2001/XMLSchema">
      <EntityMembersGetResponse xmlns="http://schemas.microsoft.com/sqlserver/masterdataservices/2009/09">
         <EntityMembers xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <EntityId>
               <Name>Opleidingen</Name>
            </EntityId>
            <MemberType>Leaf</MemberType>
            <Members>
               <Member>
                  <AccessPermission>Update</AccessPermission>
                  <Attributes>
                     <Attribute>
                        <Identifier>
                           <Id>758da61c-7f1b-4078-a242-da577b336988</Id>
                           <Name>mutatiedatum</Name>
                        </Identifier>
                        <Type>DateTime</Type>
                        <Value i:type="a:dateTime">2015-08-28T00:00:00</Value>
                     </Attribute>
                     <Attribute>
                        <Identifier>
                           <Id>a773e70c-7b35-468f-80c7-23006d154f25</Id>
                           <Name>naam opleiding</Name>
                        </Identifier>
                        <Type>String</Type>
                        <Value i:type="a:string">B Chemie</Value>
                     </Attribute>
                     <Attribute>
                        <Identifier>
                           <Id>8e2cc3ca-f213-4158-b0a9-65fcba85e847</Id>
                           <Name>afkorting opleiding</Name>
                        </Identifier>
                        <Type>String</Type>
                        <Value i:type="a:string">C</Value>
                     </Attribute>
                     <!-- ... meer attribute-objecten conform mapping ... -->
                  </Attributes>
                  <AuditInfo>
                     <CreatedDateTime>2018-04-09T15:15:38.76</CreatedDateTime>
                     <CreatedUserId/>
                     <RevisionId>272018</RevisionId>
                     <UpdatedDateTime>2018-08-07T11:51:03.593</UpdatedDateTime>
                     <UpdatedUserId>
                        <Name>DOMAIN\bbe15</Name>
                     </UpdatedUserId>
                  </AuditInfo>
                  <MemberId>
                     <Id>60989517-e91e-49d8-8563-65409391e914</Id>
                     <InternalId>1</InternalId>
                     <Code>1</Code>
                     <MemberType>Leaf</MemberType>
                  </MemberId>
                  <SecurityPermission>Access</SecurityPermission>
                  <ValidationStatus>ValidationSucceeded</ValidationStatus>
               </Member>
            </Members>
            <ModelId>
               <Name>Things</Name>
            </ModelId>
            <VersionId>
               <Name>VERSION_1</Name>
            </VersionId>
         </EntityMembers>
         <EntityMembersInformation xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <MemberCount>1</MemberCount>
            <PageNumber>1</PageNumber>
            <TotalMemberCount>0</TotalMemberCount>
            <TotalPages>0</TotalPages>
         </EntityMembersInformation>
         <OperationResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
            <Errors/>
            <RequestId>89c3dc7e-50e5-40dc-b301-c2c5c8a22eda</RequestId>
         </OperationResult>
      </EntityMembersGetResponse>
   </s:Body>
</s:Envelope>
```

**Response (OOAPI)**

```json
{
  "educationalProgrammeId": "50002140",
  "name": "B Chemie",
  "description": "",
  "termStartDate": "2011-09-01",
  "termEndDate": "",
  "ects": 0,
  "mainLanguage": "nl-NL",
  "qualificationAwarded": "Bachelor of Science",
  "lenghtOfProgramme": 0,
  "levelOfQualification": "",
  "fieldsOfStudy": "Techniek",
  "profileOfProgramme": "",
  "programmeLearningOutcomes": "",
  "modeOfStudy": "voltijd",
  "_extend": {
    "code": "C1VT"
  },
  "_links": {
    "self": {
      "href": "educational-programmes/50002140"
    },
    "courses": {
      "href": "courses?educationalProgramme=50002140"
    }
  }
}
```



## Mapping

In de onderstaande mapping is in de kolom 'Bron: MDS' het `Id` van het attribuut vermeld van de `Member` uit de response van MDS. De waarde in het veld `Value` dient gemapped te worden aan het OOAPI veld.

| Bron: MDS (`Attribute/Identifier/Id`)                        | Datatransformatie                                     | OOAPI Veld                  |
| ------------------------------------------------------------ | ----------------------------------------------------- | --------------------------- |
| `41bc8e6b-1f54-4538-b194-eb573203edbc` (Object-Id)           |                                                       | `educationalProgrammeId`    |
| `a773e70c-7b35-468f-80c7-23006d154f25` (naam opleiding)      |                                                       | `name`                      |
|                                                              | Vaste waarde "" (leeg)                                | `description`               |
| `a83330d3-18dc-4f0d-a738-efdb74f19eb3` (datum begin instroom opleiding) | `dateTime` -> `date`                                  | `termStartDate`             |
| `02d7070c-57a6-44a5-a683-7e25ae28feee` (datum einde opleiding) | `dateTime` -> `date`                                  | `termEndDate`               |
| `fcc9fb42-3e70-40a9-b76d-f5640d5b0226` (studielast)          |                                                       | `ects`                      |
| `3c163048-3cae-4d5f-95fc-11f23e1cd1cd` (voertaal)            | Omzetten naar locale. Bv: `Nederlands` > `nl_NL`      | `mainLanguage`              |
| `b9fed69e-72aa-45fd-84b6-3352b71ec54f` (actueel geldende graad met toevoeging) |                                                       | `qualificationAwarded`      |
| `df3b695c-2d27-46dc-84f3-c5fc1371586e` (nominale studielast in jaren) | Jaren omzetten naar maanden (* 12)                    | `lenghtOfProgramme`         |
| `06722cbf-2a56-43e1-80e2-1059ad6da1d9` (onderwijssoort)      |                                                       | `levelOfQualification`      |
| `6d40ad65-762f-46bf-8d35-b80a1373b004` (onderdeel)           |                                                       | `fieldsOfStudy`             |
| `76ee4141-0f85-4d16-aea6-a01c4e2ed367` (omschrijving)        |                                                       | `profileOfProgramme`        |
| `100b6854-d34c-4706-9f5c-83751df127cf` (Learning Outcomes)   |                                                       | `programmeLearningOutcomes` |
| `a6a7ec40-c9e1-4838-99ba-67b182195afe` (onderwijsvorm)       |                                                       | `modeOfStudy`               |
| `8dec398d-2685-47dc-84fd-892122ff3a4a` (Objectcodes)         |                                                       | `_extend/code`              |
| `41bc8e6b-1f54-4538-b194-eb573203edbc`(Object-Id)            | Samenvoegen naar: `educational-programmes/{id}`       | `_links/self/href`          |
| `41bc8e6b-1f54-4538-b194-eb573203edbc`(Object-Id)            | Samenvoegen naar: `courses?educationalProgramme={id}` | `_links/courses/href`       |

