![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

# OOAPI: institution

```sequence
participant Client
participant APIGW
participant MDS

Client->APIGW: GET: ooapi/institution
APIGW->MDS: GetInstitutionData()
MDS-->APIGW: Resonse(institution)
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(institution)
```

**Endpoint:** `/institution`
**Authenticatie:** nee

De institution endpoint geeft standaard informatie terug over Hogeschool Saxion. Deze informatie wordt middels MDS beheerd en is middels deze endpoint van OOAPI op te vragen.

##  Voorbeeld bericht

 **Request (Client->OOAPI)**
```http
GET https://.../ooapi/institution HTTP/1.1
Accept: application/json
```

**Request (APIGW->MDS)**
```xml
<?xml>
```

**Response (OOAPI)**

```json
{
    "institutionId": "SAX",
    "brin": "23AH",
    "name": "Saxion Hogeschool",
    "description": "Saxion is een hogeschool voor het hoger beroepsonderwijs (hbo). In Apeldoorn, Deventer en Enschede studeren ruim 26.000 studenten. Saxion biedt uiteenlopende opleidingen en bijzondere afstudeerrichtingen of specialisaties, zowel nationaal als internationaal. Samen met bedrijven en instellingen wordt door onze lectoraten toegepast onderzoek gedaan. Daarnaast heeft onze Parttime School een gevarieerd aanbod van bij- en nascholing.",
    "address": {
        "street": "M.H. Tromplaan 28",
        "additional": "Ko Wierenga",
        "city": "Enschede",
        "zip": "7513 AB",
        "countryCode": "NL"
    },
    "logo": "https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png",
    "_links": {
        "self": {
            "href": "/institution"
        },
        "educational-programmes": {
            "href": "/educational-programmes"
        }
    }
}
```

## Mapping

*Totdat een duidelijke bron is ingericht (MDS), zal de data uit ESB variabelen komen waarin de waarden zoals bovenstaand voorbeeld initieel gebruikt kunnen worden.*

| Bron (MDS) | Datatransformatie                       | OOAPI Veld                           |
| ---------- | --------------------------------------- | ------------------------------------ |
|            |                                         | `institutionId`                      |
|            |                                         | `brin`                               |
|            |                                         | `name`                               |
|            |                                         | `description`                        |
|            |                                         | `address/street`                     |
|            |                                         | `address/additional`                 |
|            |                                         | `address/city`                       |
|            |                                         | `address/zip`                        |
|            |                                         | `address/countryCode`                |
|            |                                         | `logo`                               |
|            | Vaste waarde: `/institution`            | `_links/self/href`                   |
|            | Vaste waarde: `/educational-programmes` | `_links/educational-programmes/href` |