![](https://www.saxion.nl/binaries/content/assets/over-saxion/organisatie/toolkit/lg_saxion_rgb.png)

# OOAPI: courses

**Endpoint-list:** `/courses`
**Endpoint-single**: `/courses/{courseId}`
**Authenticatie:** ja

De course endpoints geven courses (leereenheden of modules) terug op basis van eventueel meegegeven filters.

## Ophalen lijst

```sequence
participant Client
participant APIGW
participant ODS
participant MDS

Client->APIGW: GET: ooapi/courses
APIGW->ODS: Query: courses
ODS-->APIGW: Query-result
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(courses)
```

De volgende query parameters worden ondersteund (conform OOAPI specificatie):

- `pageSize`
- `pageNumber`
- ~~`faculty`~~ *(later te implementeren)*
- `educationalProgramme`
- `courseGroup`

Standaard sortering: `id`

**Request (Client->OOAPI)**

```http
GET https://.../ooapi/courses/?pageSize=100&pageNumber=2 HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Query (OOAPI->ODS (`course`))**

```mssql
SELECT DISTINCT TOP 100 c.*,
	formats = STUFF((SELECT ',' + format FROM oc.course_format AS cf WHERE cf.course_id = c.id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
FROM oc.course AS c
	LEFT JOIN oc.course_course_group AS ccg ON ccg.course_id = c.id
		LEFT JOIN oc.course_group_sor_opleiding AS cgso ON cgso.course_group_id = ccg.course_group_id
WHERE 1 = 1
-- Optioneel filter educationalProgramme
AND cgso.sor_opleiding_id = '50002083'
-- Optioneel filter courseGroup
AND ccg.course_group_id = '076e2030-ce63-11e5-95a1-24ee20524153'
ORDER BY co.id
```

**Query-result**

```
id                                  |name               
------------------------------------|------------------- ...
01f8ab2d-6edf-44b4-99f5-c692f326f0e2|Bedrijfsadministrat ...
02a4cbdc-a61e-43cf-9b34-ae79926f256d|Bestuurlijke inform ... 
...
```

**Response (OOAPI)**

- `_embedded/items` worden gemapped conform mapping.
- `_links` worden gevuld met automatisch gegenereerde href's conform paginering. Hierin is (volgens OOAPI specs) een `self`, `next` en `prev` object mogelijk, afhankelijk van de beschikbare pagina’s. Genoemde href’s zijn relatief aan het vaste gedeelte van het endpoint. Alle query parameters van de originele request dienen verwerkt te worden in de nieuwe href’s.

```json
{
  "pageSize": "250",
  "pageNumber": "1",
  "_embedded": {
    "items": [
      {
        "courseId": "01f8ab2d-6edf-44b4-99f5-c692f326f0e2",
        "name": "Bedrijfsadministratie 2.3 & 2.4",
        "abbreviation": "L.11841",
        "ects": 0,
        "description": "Zie Blackboard",
        "learningOutcomes": "",
        "goals": "",
        "requirements": "",
        "level": "",
        "format": [
          "Practicum",
          "Hoorcollege",
          "Werkcollege"
        ],
        "modeOfDelivery": "",
        "mainLanguage": "nl-NL",
        "enrollment": "",
        "resources": "",
        "exams": "",
        "schedule": "",
        "link": "",
        "_links": {
          "self": {
            "href": "courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
          },
          "courseOfferings": {
            "href": "course-offerings/?course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
          }
        }
      },
... (NOG MAXIMAAL 249 ITEMS) ...
    ]
  },
  "_links": {
    "self": {
      "href": "courses/?pageSize=250"
    },
    "next": {
      "href": "courses/?pageSize=250&pageNumber=2"
    }
  }
}
```

## Ophalen enkel record

```sequence
participant Client
participant APIGW
participant ODS
participant MDS

Client->APIGW: GET: ooapi/courses/{courseId}
APIGW->ODS: Query: course-by-id
ODS-->APIGW: Query-result
APIGW->ODS: Query: sor_opleiding_id-by-course_id
ODS-->APIGW: Query-result
note over APIGW,MDS: async-loop: Voor elke sor_opleiding_id
    APIGW->MDS: EntityMembersGet(sor_opleiding_id)
    MDS-->APIGW: Response(opleiding)
note over APIGW,MDS: end-loop
APIGW->ODS: Query: course_groups-by-course_id
ODS-->APIGW: Query-result
APIGW->APIGW: OOAPI-Mapping
APIGW-->Client: Response(course)
```

**Request (Client-OOAPI)**

```http
GET https://.../ooapi/courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2 HTTP/1.1
Accept: application/json
Authorization: Basic ****
```

**Query (OOAPI->ODS (`course`))**

```mssql
SELECT c.*,
	formats = STUFF((SELECT ',' + format FROM oc.course_format AS cf WHERE cf.course_id = c.id FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
FROM oc.course AS c
WHERE c.id = '01f8ab2d-6edf-44b4-99f5-c692f326f0e2'
```

**Query-result**

```
id                                  |name                           
------------------------------------|--------------------------- ...
01f8ab2d-6edf-44b4-99f5-c692f326f0e2|Bedrijfsadministratie 2.3 & ...
```

**Query (OOAPI->ODS (sor_opleiding))**
In het `course`-object is een lijst opgenomen met een `title` en een `link` van de `educationalProgrammes` waaraan de `course` gekoppeld is. Met deze query is voor de betreffende `course` deze lijst met `educationalProgrammes` op te halen.

```mssql
SELECT sor_opleiding_id
FROM oc.course_group_sor_opleiding AS cgso
	JOIN oc.course_course_group AS ccg ON ccg.course_group_id = cgso.course_group_id
		AND ccg.course_id = '01f8ab2d-6edf-44b4-99f5-c692f326f0e2'
```

**Query-result**

```
sor_opleiding_id|
----------------|
50002083        |
50002084        |
```

**Request (OOAPI->MDS (`educationalProgramme`))**
Per sor_opleiding_id kan de titel van de opleiding opgehaald worden uit MDS. Aangezien het vaak meerdere opleidingen zijn, is het inzake performance aan te raden dat de opleidingen asynchroon worden opgehaald uit MDS.

[Zie voorbeeldberichten van educational-programmes](./endpoint-educational-programmes.md)

**Query (OOAPI->ODS (`course_groups`))**

In het `course`-object is een lijst opgenomen met een `title` en een `link` van de `courseGroups` waaraan de `course `gekoppeld is. Met deze query is voor de betreffende `course` deze lijst met `courseGroups` op te halen.

```mssql
SELECT cg.id, cg.name, cg.description
FROM oc.course_group AS cg
	JOIN oc.course_course_group AS ccg ON ccg.course_group_id = cg.id
		AND ccg.course_id = '01f8ab2d-6edf-44b4-99f5-c692f326f0e2'
```

**Query-result**

```
id                                  |name   |description|
------------------------------------|-------|-----------|
076e2030-ce63-11e5-95a1-24ee20524153|CL.6703|AC jaar 2  |
```

**Response (OOAPI)**
Genoemde href’s zijn relatief aan het vaste gedeelte van het endpoint.

```json
{
  "courseId": "01f8ab2d-6edf-44b4-99f5-c692f326f0e2",
  "name": "Bedrijfsadministratie 2.3 & 2.4",
  "abbreviation": "L.11841",
  "ects": 0,
  "description": "Zie Blackboard",
  "learningOutcomes": "",
  "goals": "",
  "requirements": "",
  "level": "",
  "format": [
    "Practicum",
    "Hoorcollege",
    "Werkcollege"
  ],
  "modeOfDelivery": "",
  "mainLanguage": "nl-NL",
  "enrollment": "",
  "resources": "",
  "exams": "",
  "schedule": "",
  "link": "",
  "_links": {
    "self": {
      "href": "courses/01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
    },
    "courseOfferings": {
      "href": "coure-offerings/?course=01f8ab2d-6edf-44b4-99f5-c692f326f0e2"
    },
    "educationalProgrammes": [
      {
        "href": "educational-programmes/50002083",
        "title": "50002083"
      },
      {
        "href": "educational-programmes/50002084",
        "title": "50002084"
      }
    ],
    "courseGroup": [
      {
        "href": "course-groups/076e2030-ce63-11e5-95a1-24ee20524153",
        "title": "CL.6703 - AC jaar 2"
      }
    ]
  }
}
```

## Mapping

| Bron (ODS/MDS)                                               | Datatransformatie                                       | OOAPI Veld                             |
| ------------------------------------------------------------ | ------------------------------------------------------- | -------------------------------------- |
| `id`                                                         |                                                         | `courseId`                             |
| `name`                                                       |                                                         | `name`                                 |
| `abbreviation`                                               |                                                         | `abbreviation`                         |
| `ects`                                                       |                                                         | `ects`                                 |
| `description`                                                |                                                         | `description`                          |
| `learning_outcomes`                                          |                                                         | `learningOutcomes`                     |
| `goals`                                                      |                                                         | `goals`                                |
| `requirements`                                               |                                                         | `requirements`                         |
| `level`                                                      |                                                         | `level`                                |
| `formats`                                                    | Split op komma (,): omzetten naar JSON array            | `format`                               |
| `mode_of_delivery`                                           |                                                         | `modeOfDelivery`                       |
| `main_language`                                              |                                                         | `mainLanguage`                         |
| `enrollment`                                                 |                                                         | `enrollment`                           |
| `resources`                                                  |                                                         | `resources`                            |
| `exams`                                                      |                                                         | `exams`                                |
| `schedule`                                                   |                                                         | `schedule`                             |
|                                                              | Standaard waarde “” (leeg)                              | `link`                                 |
| `id`                                                         | Samenvoegen `courses/{id}`                              | `_links/self/href`                     |
| `id`                                                         | Samenvoegen `course-offerings/?course={id}`             | `_links/courseOfferings/href`          |
| `sor_opleidingen/*/sor_opleiding_id`                         | Samenvoegen `educational-programmes/{sor_opleiding_id}` | `_links/educationalProgrammes/*/href`  |
| MDS: `a773e70c-7b35-468f-80c7-23006d154f25` (naam opleiding) |                                                         | `_links/educationalProgrammes/*/title` |
| `course-groups/*/id`                                         |                                                         | `_links/courseGroups/*/href`           |
| `course-groups/*/name`, `course-groups/*/description`        | Samenvoegen `{name} - {description}`                    | `_links/courseGroups/*/title`          |

